//PARTIE REUNI
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>

 using namespace cv ;
 using namespace std ;

 void help ()
 {
  cout << " \n This program demonstrates line finding with the Hough transform. \n "
          "Usage: \n "
          "./houghlines <image_name>, Default is pic1.jpg \n " << endl ;
 }


 int main ( int argc , char ** argv )
 {

  const char * filename = argc >= 2 ?  argv [ 1 ] : "piece_1_euro.jpg" ;

  Mat src = imread ( filename , 0 );
  if ( src . empty ())
  {
      help ();
      cout << "can not open " << filename << endl ;
      return - 1 ;
  }

  Mat dst , cdst , dst_polar, cdst_polar;

  ///Utilisation de la fonction canny pour dissocier les couleurs
  Canny ( src , dst , 50 , 200 , 3 );

  /// Convertir l'image en niveaux de gris
  cvtColor ( dst , cdst , CV_GRAY2BGR );

  /// appel de la fonction cvLinearPolar
    Point2f center( (float)cdst.cols / 2, (float)cdst.rows / 2 );
    double radius = 90;
    double M = (double)cdst.cols / log(radius);

    //logPolar(frame,log_polar_img, center, M,);
    linearPolar(cdst,cdst_polar, center, radius, INTER_LINEAR + WARP_FILL_OUTLIERS);


    imshow ( "image source" , src );
    imshow ( "detected lines" , cdst );
    imshow ( "cdst_polar" , cdst_polar );

    waitKey();
  return 0 ;
 }
