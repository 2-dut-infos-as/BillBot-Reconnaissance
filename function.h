#ifndef FUNCTION_H_INCLUDED
#define FUNCTION_H_INCLUDED

int getRayon(double, double);
int getAngle(double);
void help ();
double** transformation(cv :: Mat,double,double , double);
double ** imageToTabUnePiece (char*) ;
double convertMatToDouble(cv::Mat);

#endif // FUNCTION_H_INCLUDED
