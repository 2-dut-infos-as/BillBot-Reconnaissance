
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <math.h>
#include "function.h"

#include <iostream>

 using namespace cv ;
 using namespace std ;
 ///-------------------------------------------------------------------------------------------------
 ///    SOUS PROGRAMME ALERTE
 ///--------------------------------------------------------------------------------------------------

void help ()
 {
  cout << " \n Ce programme d�montre la recherche de ligne avec la transformation de Hough. \n "
          "Usage: \n "
          "./houghlines <image_name>, Default is piece_1_euro.jpg \n " << endl ;
 }



 ///-------------------------------------------------------------------------------------------------
 ///    FIN SOUS PROGRAMME ALERTE
 ///--------------------------------------------------------------------------------------------------


double convertMatToDouble(cv::Mat theta){
  std:: vector <int> array(theta.rows*theta.cols);
            if (theta.isContinuous()){



            double *ptrDst = theta.ptr<double>();
            for(int i = 0; i < theta.total(); ++i) {
                    double value = ptrDst[i]; // Or do some other stuff
                    //cout<< "la valeur est "<<value<<endl;
                    return value;
            }
        } else{
            for(int i = 0; i < theta.rows; ++i) {
                double *ptrDst = theta.ptr<double>(i);

                for(int j = 0; j < theta.cols; ++j) {
                    double value = ptrDst[j];
                     // cout<< "la valeur est "<<value<<endl;
                      return value;
                }
            }
        }

}
int getRayon(double distance, double rayon ){

   return distance /(rayon/10);

}

int getAngle(double theta){
    return theta/10 ;
}

double** imageToTabUnePiece (char* link){



    //--------------------------------------------------------------------------------------------------------------------
    //Variable Global
    //--------------------------------------------------------------------------------------------------------------------
    int ray_div ;
    int tab_ray[20];
    int k = 0;
    double theta1,theta2;
    double i1,j1, i2,j2;
    double ray;
    int centre_x,centre_y;
    double coord_rayon;
    double rayon;
    cv::Scalar color1;




    ///Varible pour les pixel
    int _blanc=255;
    int cpt = 0;

    ///vecteur
    vector<Vec3f> circles;

    ///D�claration de matrice
    Mat src_gray , canny_src ,canny_src_copie , canny_src_polar;
    Mat centre_mat;

    /// Image en niveaux de gris � canal unique (type 8UC1) Et les coordonn�es des pixels x=5 and y=2
    /// par convention, {ligne = y} et {colonne = x}
    /// intensity.val[0] contient une valeur entre 0 et 255
    Scalar intensity1;
    //----------------------------------------------------------------------------------------------------------------------------
    //      FIN DECLARATION
    //----------------------------------------------------------------------------------------------------------------------------

    //------------------------------------------------------------------------------------------------------------------------------
    //    APPEL DE L'iMAGE
    //-----------------------------------------------------------------------------------------------------------------------------
    ///Charge l'image
   // const char * filename = argc >= 2 ?  argv [ 1 ] : "images/piece_1_euro.jpg" ;
    const char * filename = link ;
    ///Lecture de l'image vers une matrice
    Mat src = imread ( filename , 0 );

    //-----------------------------------
    // TRAITEMENT DES EXCEPTION
    //-------------------------------------
    //Si vide alors message d'erreur
   /* if ( src . empty ())
      {
          help ();
          cout << "can not open " << filename << endl ;
          return nullptr ;
      }
*/

    //---------------------------------------------------------------------------------------------------------------------------------
    //      PROGRAMME
    //-------------------------------------------------------------------------------------------------------------------------------

    ///Utilisation de la fonction canny pour dissocier les couleurs
    Canny ( src , src_gray , 50 , 200 , 3 );

    /// Convertir l'image en niveaux de gris
    cvtColor ( src_gray , canny_src , CV_GRAY2BGR );
    cvtColor ( src_gray , canny_src_copie , CV_GRAY2BGR );




    /// conna�tre le nombre de canaux que l'image a
    cout<<"Canaux d'image originale: "<<src_gray.channels()<<"   Canaux d'image grise: "<<canny_src.channels()<<endl;


    /// R�duisez le bruit afin d'�viter la d�tection de faux cercle
    GaussianBlur( src_gray, src_gray, Size(9, 9), 2, 2 );

    /// Applique la transform� de hough pour trouver un cercle
    HoughCircles( src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows/8, 200, 100, 0, 0 );

    /// On travail dans le cercles(Pourra �tre remplacer par une appel de fonction via la detection)
    for( size_t i = 0; i < circles.size(); i++ )
      {
        //Declaration du centre de l'image;
        Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));

        rayon = cvRound(circles[i][2]);
        coord_rayon=rayon;
        double M = (double)canny_src.cols / log(rayon);

        ///r�cup�re les coordonn�e du centre de l'image
        centre_x = center.x;
        centre_y = center.y;



        ///Dessine deux cercle ==> rep�rage du centre et contour de la pi�ce
        // circle center
        circle( canny_src_copie, center, 3, Scalar(0,255,0), -1, 8, 0 );
        // circle outline
        circle( canny_src_copie, center, rayon, Scalar(0,0,255), 2, 8, 0 );

        ///Appel de la fonction Line ==> permet de voir notre d�coupage du cercle

        line(canny_src_copie, center, Point( 54, 90), Scalar( 110, 220, 0 ), 2, 8 );
        line(canny_src_copie, center, Point( 28, 190),Scalar( 110, 220, 0 ), 2, 8 );

        cout<<"centre : " <<center <<endl;
        cout<<"rayon : " <<rayon <<endl;

    }


    imshow ( "image source" , canny_src );

   return transformation(canny_src,centre_x,centre_y, rayon);


}


 ///------------------------------------------------------------------------------------------------------
 ///        SOUS PROGRAMME TRANSFORMATION
 ///-----------------------------------------------------------------------------------------------------
 double** transformation(Mat image,double ctr_x,double ctr_y, double rayon){

//-------------------------------------------------------------------------------------------
//      DECLARATION DES VARIABLES LOCAL
//-----------------------------------------------------------------------------------------
    cv::Mat distancePc;
    cv::Mat theta;

    int div=10;
	double** tab = new double*[36];
	for (int i = 0; i<36; i++)
        tab[i] = new double[div];

    for (int i = 0; i<36; i++){
            for (int j = 0; j<10; j++){
                tab[i][j] = 0;
            }
         }
	//double ecart = ligne/div;
    double coord_x;
    double coord_y;

    int cadrant;

    double ecart = ctr_x/div;
    cv::Scalar pointeur;
    double dist;

//------------------------------------------------------------------------------------------
//      PROGRAMME
//--------------------------------------------------------------------------------------------
	//1er quart
	for(int Points_x=0;Points_x<image.cols;Points_x++){
		for(int Points_y=0;Points_y<image.rows;Points_y++){

            coord_x =  Points_x - ctr_x;
            coord_y = Points_y - ctr_y;

            //cout<<"coord_x: "<<coord_x<<endl;
            //cout<<"coord_y: "<<coord_y<<endl;
        double longueurDuCentre = sqrt((coord_x* coord_x) + (coord_y * coord_y));

        if (longueurDuCentre <= rayon)  {
         //cout << "longueur du centre "<<longueurDuCentre<<endl;
         //cout << "rayon "<<rayon<<endl;
        cv::cartToPolar(double(coord_x), double(coord_y), distancePc, theta, true);

          double valueAngle = convertMatToDouble(theta);
         int  valueA = getAngle(valueAngle);


          double valueRayon = convertMatToDouble(distancePc);
          int valueR = getRayon(valueRayon, rayon);





         //   cout<<"rayon r du point contour :"<<point_rayon<<endl;
           // cout<<"angle theta :"<<theta<<endl;

            pointeur = image.at<uchar>(Points_x,Points_y); //recupere la valeur du pixel

			if(abs(pointeur.val[0] !=0)){
				tab[valueA][valueR]= tab[valueA][valueR]+1;
				//cout<<"tab["<<angle<<"]["<<candrant<<"]="<<tab[angle][candrant]<<endl;
		//}
	}
    /*
			//im0Bgr.convertTo(im0BgrDouble, CV_32F);

            ///incr�mentes le tableau (dont chaque case vaut 0 au d�but de l'algo) � la position R, T


			}

		}
		*/

	}
	}
}
///Affichage des pixel compter dans le tableau
for (int i = 0; i<36; i++){
            for (int j = 0; j<10; j++){
              cout<< "tab["<<i<<"]["<<j<<"] = "<< tab[i][j]<<endl;
            }
         }
         return tab;

  //  Mat data_pts ;

  //  PCA pca_analysis(data_pts, Mat(), CV_PCA_DATA_AS_ROW);
}

///-------------------------------------------------------------------------------------------------------------------------
///         PROGRAMME PRINCIPAL
///--------------------------------------------------------------------------------------------------------------------------
